
% run tests: swipl -q -s citeman_tests.pl -t run_tests

:- use_module(library(test_cover)).
:- begin_tests(citeman).
:- [citeman].

test(mla1) :-
    citation(Style, A, T, Y, J, V, PS, PE, "Mesoudi, Alex, Andrew Whiten, and Kevin N. Laland. \"Perspective: Is human cultural evolution Darwinian? Evidence reviewed from the perspective of The Origin of Species.\" Evolution 58.1 (2004): 1-11."),
    Style = mla,
    A = [("Mesoudi", "Alex"), ("Whiten", "Andrew"), ("Laland", "Kevin N.")],
    T = "Perspective: Is human cultural evolution Darwinian? Evidence reviewed from the perspective of The Origin of Species",
    Y = 2004,
    J = "Evolution",
    V = (58, 1),
    PS = 1,
    PE = 11.

test(mla2) :-
    citation(Style, A, T, Y, J, V, PS, PE, "Mesoudi, Alex, Andrew Whiten, and Kevin N. Laland. \"Perspective: Is human cultural evolution Darwinian? Evidence reviewed from the perspective of The Origin of Species.\" Evolution 58 (2004): 1-11."),
    Style = mla,
    A = [("Mesoudi", "Alex"), ("Whiten", "Andrew"), ("Laland", "Kevin N.")],
    T = "Perspective: Is human cultural evolution Darwinian? Evidence reviewed from the perspective of The Origin of Species",
    Y = 2004,
    J = "Evolution",
    V = (58, noissue),
    PS = 1,
    PE = 11.

test(apa1) :-
    citation(Style, A, T, Y, J, V, PS, PE, "Doe, D. X., & Smith, P. (2009). Test title. This Journal, 15(2), 55-99."),
    Style = apa,
    A = [("Doe", "D. X."), ("Smith", "P.")],
    T = "Test title",
    Y = 2009,
    J = "This Journal",
    V = (15, 2),
    PS = 55,
    PE = 99.

test(apa2) :-
    citation(Style, A, T, Y, J, V, PS, PE, "Doe, D. X., & Smith, P. (2009). Test title. This Journal, 15, 55-99."),
    Style = apa,
    A = [("Doe", "D. X."), ("Smith", "P.")],
    T = "Test title",
    Y = 2009,
    J = "This Journal",
    V = (15, noissue),
    PS = 55,
    PE = 99.

test(apa3) :-
    citation(Style, A, T, Y, J, V, PS, PE, "Doe, D. X. (2009). Test title. This Journal, 15, 55-99."),
    Style = apa,
    A = [("Doe", "D. X.")],
    T = "Test title",
    Y = 2009,
    J = "This Journal",
    V = (15, noissue),
    PS = 55,
    PE = 99.

test(chicago1) :-
    citation(Style, A, T, Y, J, V, PS, PE, "Mesoudi, Alex, Andrew Whiten, and Kevin N. Laland. \"Perspective: Is human cultural evolution Darwinian? Evidence reviewed from the perspective of The Origin of Species.\" Evolution 58, no. 1 (2004): 1-11."),
    Style = chicago,
    A = [("Mesoudi", "Alex"), ("Whiten", "Andrew"), ("Laland", "Kevin N.")],
    T = "Perspective: Is human cultural evolution Darwinian? Evidence reviewed from the perspective of The Origin of Species",
    Y = 2004,
    J = "Evolution",
    V = (58, 1),
    PS = 1,
    PE = 11.

test(chicago2) :-
    citation(Style, A, T, Y, J, V, PS, PE, "Mesoudi, Alex, Andrew Whiten, and Kevin N. Laland. \"Perspective: Is human cultural evolution Darwinian? Evidence reviewed from the perspective of The Origin of Species.\" Evolution 58 (2004): 1-11."),
    member(Style, [chicago, mla]), % ambiguous if journal issue is missing
    A = [("Mesoudi", "Alex"), ("Whiten", "Andrew"), ("Laland", "Kevin N.")],
    T = "Perspective: Is human cultural evolution Darwinian? Evidence reviewed from the perspective of The Origin of Species",
    Y = 2004,
    J = "Evolution",
    V = (58, noissue),
    PS = 1,
    PE = 11.

test(chicago3) :-
    citation(Style, A, T, Y, J, V, PS, PE, "Doe, John. \"Test title.\" This Journal 15, no. 2 (2009): 55-99."),
    Style = chicago,
    A = [("Doe", "John")],
    T = "Test title",
    Y = 2009,
    J = "This Journal",
    V = (15, 2),
    PS = 55,
    PE = 99.

test(harvard1) :-
    citation(Style, A, T, Y, J, V, PS, PE, "Mesoudi, A., Whiten, A. and Laland, K.N., 2004. Perspective: Is human cultural evolution Darwinian? Evidence reviewed from the perspective of The Origin of Species. Evolution, 58(1), pp.1-11."),
    Style = harvard,
    A = [("Mesoudi", "A."), ("Whiten", "A."), ("Laland", "K.N.")],
    T = "Perspective: Is human cultural evolution Darwinian? Evidence reviewed from the perspective of The Origin of Species",
    Y = 2004,
    J = "Evolution",
    V = (58, 1),
    PS = 1,
    PE = 11.

test(harvard2) :-
    citation(Style, A, T, Y, J, V, PS, PE, "Mesoudi, A., Whiten, A. and Laland, K.N., 2004. Perspective: Is human cultural evolution Darwinian? Evidence reviewed from the perspective of The Origin of Species. Evolution, 58, pp.1-11."),
    Style = harvard,
    A = [("Mesoudi", "A."), ("Whiten", "A."), ("Laland", "K.N.")],
    T = "Perspective: Is human cultural evolution Darwinian? Evidence reviewed from the perspective of The Origin of Species",
    Y = 2004,
    J = "Evolution",
    V = (58, noissue),
    PS = 1,
    PE = 11.

test(harvard3) :-
    citation(Style, A, T, Y, J, V, PS, PE, "Doe, John, 2009. Test title. This Journal, 15, pp.55-99."),
    Style = harvard,
    A = [("Doe", "John")],
    T = "Test title",
    Y = 2009,
    J = "This Journal",
    V = (15, noissue),
    PS = 55,
    PE = 99.

test(render1) :-
    citation(Style, A, T, Y, J, V, PS, PE, "Doe, John, 2009. Test title. This Journal, 15, pp.55-99."),
    renderCitationsStyle(mla, [(Style,A,T,Y,J,V,PS,PE)], [S]),
    S = "Doe, John. \"Test title.\" This Journal 15 (2009): 55-99.".

test(render2) :-
    citation(Style, A, T, Y, J, V, PS, PE, "Richardson, J. E., Weltz, F. M., Fay, M. F., & Cronk, Q. C. (2001). Rapid and recent origin of species richness in the Cape flora of South Africa. Nature, 412(6843), 181-191."),
    renderCitationsStyle(harvard, [(Style,A,T,Y,J,V,PS,PE)], [S]),
    S = "Richardson, J. E., Weltz, F. M., Fay, M. F. and Cronk, Q. C., 2001. Rapid and recent origin of species richness in the Cape flora of South Africa. Nature, 412(6843), pp.181-191.".

% test that input string is same as output string if citation style is the same
test(render3) :-
    citation(Style, A, T, Y, J, V, PS, PE, "Richardson, J. E., Weltz, F. M., Fay, M. F. and Cronk, Q. C., 2001. Rapid and recent origin of species richness in the Cape flora of South Africa. Nature, 412(6843), pp.181-191."),
    renderCitationsStyle(harvard, [(Style,A,T,Y,J,V,PS,PE)], [S]),
    S = "Richardson, J. E., Weltz, F. M., Fay, M. F. and Cronk, Q. C., 2001. Rapid and recent origin of species richness in the Cape flora of South Africa. Nature, 412(6843), pp.181-191.".

:- end_tests(citeman).


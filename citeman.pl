% citation manager
% parses and generates APA-style citations

:- set_prolog_flag(double_quotes, chars).
:- use_module(library(clpfd)).

% we're going to ignore the requirement that author names in APA and Harvard must use
% only initials for firstname; rather, APA+Harvard have format Lastname, Firstname for
% each author, while MLA has format Lastname, Firstname for first author, and
% Firstname Lastname for rest of authors; also take note of & vs. and in the different
% citation formats.
%
% additionally, italicized journal names are not italicized in our code, they're just
% regular text.
%
% finally, don't worry about titles ending with ? or similar punctuation; none of the
% test cases have such titles.


% TODO: grammar rules (my solution had 34 lines, including blanks)
% e.g., citation(apa, Authors, Title, Year, Journal, Volume, PageStart, PageEnd) --> ...
% e.g., citation(mla, Authors, Title, Year, Journal, Volume, PageStart, PageEnd) --> ...
% ...
% Refer to text, textNoSpace, year, & num grammar predicates below.

authorSingleFirstFirst((Last, First)) -->
	text(First), " ", textNoSpace(Last).

authorSingleLastFirst((Last, First)) -->
	textNoSpace(Last), ", ", text(First).

authors(mla, [Author]) -->	
	authorSingleLastFirst(Author).

authors(mla, [ Author | Authors]) -->
	authorSingleLastFirst(Author),
	", ",
	authorFirst(Authors).

authors(apa, [Author]) -->
	authorSingleLastFirst(Author).

authors(apa, [Author | Authors]) -->
	authorSingleLastFirst(Author),
	", ",
	authorFirstAmp(Authors).

authors(chicago, [Author]) -->
	authorSingleLastFirst(Author).

authors(chicago, [Author | Authors]) -->
	authorSingleLastFirst(Author), 
	", ",
	authorFirst(Authors).

authors(harvard, [Author]) -->
	authorSingleLastFirst(Author).

authors(harvard, [Author | Authors]) -->
	authorSingleLastFirst(Author),
	authorLast(Authors).

authorLast([Author]) -->
	" and ", authorSingleLastFirst(Author).

authorLast([Author | Authors]) -->
	", ", authorSingleLastFirst(Author),
	authorLast(Authors).

authorFirstAmp([Author]) -->
	"& ", authorSingleLastFirst(Author).

authorFirstAmp([Author | Authors]) -->
	authorSingleLastFirst(Author), ", ",
	authorFirstAmp(Authors).

authorFirst([Author]) -->
	"and ", authorSingleFirstFirst(Author).

authorFirst([Author | Authors]) -->
	authorSingleFirstFirst(Author), ", ", 
	authorFirst(Authors).

%citation(Style, A, T, Y, J, V, PS, PE, "Richardson, J. E., Weltz, F. M., Fay, M. F., & Cronk, Q. C. (2001). Rapid and recent origin of species richness in the Cape flora of South Africa. Nature, 412(6843), 181-191.")


citation(mla, Authors, Title, Year, Journal, (V1, noissue), PageStart, PageEnd) -->
	authors(mla, Authors), 
	". \"", text(Title),
	".\" ", text(Journal), " ", num(V1),
	" (", year(Year), "): ", num(PageStart), "-", num(PageEnd), ".".

citation(mla, Authors, Title, Year, Journal, (V1, V2), PageStart, PageEnd) -->
	authors(mla, Authors),
	". \"", text(Title),
	".\" ", text(Journal), " ", num(V1), ".", num(V2), 
	" (", year(Year), "): ", num(PageStart), "-", num(PageEnd), ".".

citation(apa, Authors, Title, Year, Journal, (V1, noissue), PageStart, PageEnd) -->
	authors(apa, Authors),
	" (", year(Year), "). ", text(Title), ". ", text(Journal), ", ", num(V1),
	", ", num(PageStart), "-", num(PageEnd), ".".


citation(apa, Authors, Title, Year, Journal, (V1, V2), PageStart, PageEnd) -->
	authors(apa, Authors),	
	" (", year(Year), "). ", text(Title), ". ", text(Journal), ", ", num(V1),
	"(", num(V2), ")",
	", ", num(PageStart), "-", num(PageEnd), ".".

citation(chicago, Authors, Title, Year, Journal, Volume, PageStart, PageEnd) -->
	authors(chicago, Authors),	
	". \"", text(Title),
	".\" ", text(Journal), " ", num(V1), {Volume = (V1, noissue)},
	" (", year(Year), "): ", num(PageStart), "-", num(PageEnd), ".".
	

citation(chicago, Authors, Title, Year, Journal, Volume, PageStart, PageEnd) -->
	authors(chicago, Authors),
	". \"", text(Title),
	".\" ", text(Journal), " ", num(V1), ", no. ", num(V2),  {Volume = (V1, V2)},
	" (", year(Year), "): ", num(PageStart), "-", num(PageEnd), ".".
	

citation(harvard, Authors, Title, Year, Journal, Volume, PageStart, PageEnd) -->	
	authors(harvard, Authors),	
	", ", year(Year), ". ", text(Title),
	". ", text(Journal),
	", ", num(V1), {Volume = (V1, noissue)}, ", pp.", num(PageStart), "-", num(PageEnd), ".", !.

citation(harvard, Authors, Title, Year, Journal, Volume, PageStart, PageEnd) -->
	authors(harvard, Authors),
	", ", year(Year), ". ", text(Title),
	". ", text(Journal),
	", ", num(V1), "(", num(V2), ")", {Volume = (V1, V2)}, ", pp.",
	num(PageStart), "-", num(PageEnd), ".", !.





	




% END TODO

text([A|As]) --> [A], { char_type(A, alpha) ; member(A, " .-:?/")}, text(As).
text([]) --> [].

textNoSpace([A|As]) --> [A], { char_type(A, alpha) ; member(A, "-")}, textNoSpace(As).
textNoSpace([]) --> [].


% note, number_codes/2 fails if second arg is [], so we'll require there is at least
% one element in the list: [D|Digits]
year(Y) --> digits([D|Digits], 4), { number_codes(Y, [D|Digits]) }.
num(noissue).
num(N) --> digitnonzero(D), { dif(N, noissue), number_codes(N, [D]) }.
num(N) --> digitnonzero(D), digits(Digits, 4), { dif(N, noissue), number_codes(N, [D|Digits]) }.
digitnonzero(D) --> [D], { char_type(D, digit), dif(D, '0') }.
digits([D|Ds], MaxLength) -->
    { MaxLength #> 0, RestLength #= MaxLength - 1 },
    [D], { char_type(D, digit) }, digits(Ds, RestLength).
digits([], _) --> [].

citation(Style, Authors, Title, Year, Journal, Volume, PageStart, PageEnd, Citation) :-
    citation(Style, Authors, Title, Year, Journal, Volume, PageStart, PageEnd, Citation, []), !.

readDb(Filename, Citations) :-
    open(Filename, read, Stream),
    readLines(Stream, Lines),
    close(Stream), !,
    convertLinesToCitations(Lines, Citations).

readLines(Stream, []) :-
    at_end_of_stream(Stream).
readLines(Stream, [Line|Lines]) :-
    \+at_end_of_stream(Stream),
    read_line_to_string(Stream, LineString),
    string_chars(LineString, Line),
    readLines(Stream, Lines).

convertLinesToCitations([], []). 
convertLinesToCitations([Line|Lines], [Citation|Citations]) :-
    citation(Style, Authors, Title, Year, Journal, Volume, PageStart, PageEnd, Line),
    Citation = (Style, Authors, Title, Year, Journal, Volume, PageStart, PageEnd), !,
    convertLinesToCitations(Lines, Citations).
convertLinesToCitations([_|Lines], Citations) :-
    convertLinesToCitations(Lines, Citations).

searchAuthorLastName(LastName, Citations, Matches) :-
    findall(Citation, (member(Citation, Citations),
                       Citation = (_, Authors, _, _, _, _, _, _),
                       member((LastName, _), Authors)),
            Matches).

searchYearRange(YearStart, YearEnd, Citations, Matches) :-
    findall(Citation, (member(Citation, Citations),
                       Citation = (_, _, _, Year, _, _, _, _),
                       YearStart =< Year,
                       Year =< YearEnd),
            Matches).

renderCitationsStyle(Style, Citations, Rendered) :-
    findall(S, (member((_, A, T, Y, J, V, PS, PE), Citations),
                citation(Style, A, T, Y, J, V, PS, PE, S)),
            Rendered).

printRendered([]) :- nl.
printRendered([S|Rendered]) :-
    format("~n~s~n", [S]),
    printRendered(Rendered).

% example complex usage:
%   readDb('citations.txt', Citations),
%   searchYearRange(2000, 2010, Citations, MatchesYear),
%   searchAuthorLastName("Mesoudi", MatchesYear, MatchesBoth),
%   renderCitationsStyle(chicago, MatchesBoth, S),
%   printRendered(S).



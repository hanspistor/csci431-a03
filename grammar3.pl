:- set_prolog_flag(double_quotes, chars).
:- use_module(library(clpfd)).

test(Subject, Action) -->
	np(Subject), " ", vp(Action, none).

s(Subject, Action, Object, 0) -->
  np(Subject), " ", vp(Action, Object).
s(Subject, Action, Object, N) -->
  np(Subject), " ", vp(Action, Object),
  " ", num(N), " times".
vp(Action, Entity) -->
  v(Action), " ", np(Entity).
vp(Action, none) --> v(Action).
np(Entity) --> det, " ", n(Entity).
det --> "a".
det --> "the".
n(woman) --> "woman".
n(man) --> "man".
v(shoots) --> "shoots".
v(hugs) --> "hugs".

num(N) --> digitnonzero(D),
  { number_codes(N, [D]) }.
num(N) --> digitnonzero(D), digits([D2|Ds], 4),
  { number_codes(N, [D,D2|Ds]) }.

digitnonzero(D) --> [D],
  { char_type(D, digit), dif(D, '0') }.
digits([D|Ds], MaxLength) --> [D],
  { MaxLength #> 0, RestLength #= MaxLength - 1,
    char_type(D, digit) },
  digits(Ds, RestLength).
digits([], _) --> [].



